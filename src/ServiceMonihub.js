import { Global } from './Global';

let json = Global.json;
let retorno;
const dataJson = {
  'body':'',
  'mensaje':''
}

export async function logueo(email_login,password){
    json.body.inputParameters.push({ 'name': 'email_login', 'value': email_login });
    json.body.inputParameters.push({ 'name': 'password', 'value': password });
    json.body.serviceID.id = "2";    
    
    retorno = await postData(Global.url,json)
    /*.then(
      function(value){ 
        dataJson.body.push(value.body)
        dataJson.mensaje.push(value.header.messages)
      },
      function(error) { 
        console.log('Error')
       }
    )*/;    
    //console.log(retorno.body)

    return (retorno);
}


  async function postData (url = '', data = {}){
    //return new Promise((resolve, reject) => {
      let response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {'Content-Type': 'application/json'},
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data) // body data type must match "Content-Type" header
      });      
      let datos = await response.json();

      return datos;
  }
    