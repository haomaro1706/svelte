import { writable } from "svelte/store";
import { onMount } from 'svelte';

export var Global = {
	url: 'https://electroventas.monihub.com/admin_dev.php/creditosrest/exec',
	//url: 'http://192.168.1.54',
    urlServer: 'https://moneytech.monihub.com',
    urlServerTest: 'http://pruebasmt.monihub.com',  
    json: 
    {    
        "header":{
        "licenceKey":{
            "token":""
        },
        "guid":{
            "id":"1"
        },
        "messages":[]
        },
    
        "body":{
        "serviceID": {
            "id": "",
            "version": "1"
        },
        "inputParameters":[],
        "outputParameters": []
        }
    },
    sendMonihub: function (json){
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify(json);
        //console.log('=>'+json);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
          };
          //console.log(this.url+"web/admin_dev.php/creditosrest/exec");
          //onMount(async()=>{
              const response = fetch(this.url+"web/admin_dev.php/creditosrest/exec",requestOptions)
              const todo = response.json();
              console.log(todo);
          //});
    }
};	